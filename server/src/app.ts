import express from "express";
import {dbConnection} from "./config/db";

const PORT = process.env.PORT || 3000;
const app = express();

app.listen(PORT, async ()=>{
  await dbConnection.initialize();
  console.log(`Server is up and running on Port: ${PORT}`);
});
